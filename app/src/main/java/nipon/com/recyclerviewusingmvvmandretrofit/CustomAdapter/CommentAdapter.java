package nipon.com.recyclerviewusingmvvmandretrofit.CustomAdapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import nipon.com.recyclerviewusingmvvmandretrofit.Model.Comment;
import nipon.com.recyclerviewusingmvvmandretrofit.R;
import nipon.com.recyclerviewusingmvvmandretrofit.databinding.ItemCommentBinding;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {

    private List<Comment> commentList;
    private Context context;

    public CommentAdapter(List<Comment> commentList, Context context) {
        this.commentList = commentList;
        this.context = context;
    }

    @NonNull
    @Override
    public CommentAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        ItemCommentBinding itemCommentBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_comment,viewGroup,false);
        return new ViewHolder(itemCommentBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentAdapter.ViewHolder viewHolder, int i) {
        Comment comment = commentList.get(i);
        viewHolder.binding.postTV.setText(String.valueOf(comment.getPostId()));
        viewHolder.binding.nameTV.setText(comment.getName());
        viewHolder.binding.bodyTV.setText(comment.getBody());
    }

    @Override
    public int getItemCount() {
        return commentList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ItemCommentBinding binding;

        public ViewHolder(@NonNull ItemCommentBinding itemCommentBinding) {
            super(itemCommentBinding.getRoot());
            this.binding = itemCommentBinding;
        }
    }
}
