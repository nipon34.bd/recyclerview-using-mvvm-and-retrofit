package nipon.com.recyclerviewusingmvvmandretrofit.Interface;

import android.arch.lifecycle.MutableLiveData;

import java.util.List;

import nipon.com.recyclerviewusingmvvmandretrofit.Model.Comment;

public interface CommentInterface {
    MutableLiveData<List<Comment>> getAllComment();
    MutableLiveData<Boolean> getIsLoaded();
}
